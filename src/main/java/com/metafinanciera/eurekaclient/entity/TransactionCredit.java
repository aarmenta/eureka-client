package com.metafinanciera.eurekaclient.entity;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class TransactionCredit {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(nullable = false)
	private UUID processId;
	
	@Column(nullable = false)
	private String account;
	
	@Column(nullable = false)
	private String businessToken;
	
	@Column(nullable = false)
	private String pan;
	
	@Column(nullable = false)
	private String amount;
	
	@Column(nullable = false)
	private String commerceName;
	
	@Column(nullable = false)
	private String commerceId;
	
	@Column(nullable = false)
	private String messageType;
	
	@Column(nullable = false)
	private String authorizationDate;
	
	@Column(nullable = false)
	private String issuingCountry;
	
	@Column(nullable = false)
	private String issuingLanguage;
	
	@Column(nullable = false)
	private String cdt;
	
	@Column(nullable = false)
	private String cic;
	
	@Column(nullable = false)
	private String crt;
	
	@Column(nullable = false)
	private String tt;
	
	@Column(nullable = false)
	private String pco;
	
	@Column(nullable = false)
	private String pto;
	
	@Column(nullable = false)
	private String pos;
	
	@Column(nullable = false)
	private String mac;
	
	@Column(nullable = false)
	private String mad;
	
	@Column(nullable = false)
	private String mdc;
	
	@Column(nullable = false)
	private String ct;
	
	@Column(nullable = false)
	private String lp;
	
	@Column(nullable = false)
	private String ccp;
	
	@Column(nullable = false)
	private String cardSubtype;
	
	@Column(nullable = false)
	private String cardType;


	

}
