package com.metafinanciera.eurekaclient.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Account {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(nullable = false)
	private Long idPerson;
	
	@Column(nullable = false)
	private String account;
	
	@Column(nullable = false)
	private String businessToken;
	
	@Column(nullable = false)
	private String pan;
	
	@Column(nullable = false)
	private Date expirationDate;
	
	@Column(nullable = false)
	private String cvv;
	
	@Column(nullable = false)
	private Double creditLine;

}
