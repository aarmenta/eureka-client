package com.metafinanciera.eurekaclient.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.metafinanciera.eurekaclient.entity.Account;

public interface AccountRepository extends CrudRepository<Account, Long> {
	
	Optional<Account> findByAccountAndBusinessToken(String account, String businessToken);

}
