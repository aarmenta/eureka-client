package com.metafinanciera.eurekaclient.repository;

import java.util.Date;

import com.google.gson.Gson;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class AccountStatistics {
	
	private Long idPerson;
	
	private String account;
	
	private String businessToken;
	
	private String pan;
	
	private Date expirationDate;
	
	private String cvv;
	
	private Double creditLine;
	
	private String customer;
	
	private String identificationNumber;
	
	public AccountStatistics(
			Long idPerson,
			String account,
			String businessToken,
			String pan,
			Date expirationDate,
			String cvv,
			String customer,
			String identificationNumber) {
		
		this.idPerson = idPerson;
		this.account = account;
		this.businessToken = businessToken;
		this.pan = pan;
		this.expirationDate = expirationDate;
		this.cvv = cvv;
		this.customer = customer;
		this.identificationNumber = identificationNumber;
		
		
	}
	
	public String toJson() {
        return new Gson().toJson(this);
	}

}
