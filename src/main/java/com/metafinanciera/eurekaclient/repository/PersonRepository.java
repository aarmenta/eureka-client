package com.metafinanciera.eurekaclient.repository;

import org.springframework.data.repository.CrudRepository;

import com.metafinanciera.eurekaclient.entity.Person;

public interface PersonRepository extends CrudRepository<Person, Long> {
	
	public Person findByNameAndFirstSurnameAndSecondSurname(String name, String firstSurname, String secondSurname);

}
