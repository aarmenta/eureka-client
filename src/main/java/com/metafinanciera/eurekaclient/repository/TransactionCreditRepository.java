package com.metafinanciera.eurekaclient.repository;

import org.springframework.data.repository.CrudRepository;

import com.metafinanciera.eurekaclient.entity.TransactionCredit;

public interface TransactionCreditRepository extends CrudRepository<TransactionCredit, Long> {

}
