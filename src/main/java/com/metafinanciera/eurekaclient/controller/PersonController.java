package com.metafinanciera.eurekaclient.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.metafinanciera.eurekaclient.entity.Person;
import com.metafinanciera.eurekaclient.model.PersonRequest;
import com.metafinanciera.eurekaclient.repository.PersonRepository;
import com.metafinanciera.eurekaclient.service.PersonService;

import io.swagger.annotations.ApiOperation;

@RestController
public class PersonController {
	
	@Autowired
	private PersonRepository personRepository;
	
	@Autowired
	private PersonService personService;

	
	@ApiOperation(value = "Crea una persona Fintech")
	@PostMapping("/fintech/alta-persona")
	public Person createPerson(@RequestBody PersonRequest request) {
		
		Person persona = personService.createPerson(request);
		return persona;
		
	}
	
	@ApiOperation(value = "Buscar persona Fintech")
	@PostMapping("/fintech/find-persona")
	public Person findPersonByNameAndSurname(@RequestBody PersonRequest request) {
		
		return personRepository.findByNameAndFirstSurnameAndSecondSurname(request.getPerson().getName(), request.getPerson().getFirstSurname(), request.getPerson().getSecondSurname());
		
	}
	
	
	
}
