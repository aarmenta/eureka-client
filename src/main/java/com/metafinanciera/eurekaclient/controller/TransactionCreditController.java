package com.metafinanciera.eurekaclient.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.metafinanciera.eurekaclient.entity.TransactionCredit;
import com.metafinanciera.eurekaclient.model.TransactioCreditResponse;
import com.metafinanciera.eurekaclient.service.TransactionCreditService;

import io.swagger.annotations.ApiOperation;

@RestController
public class TransactionCreditController {
	
	@Autowired
	private TransactionCreditService transactionCreditService;

	@ApiOperation(value = "Informa un Transaction Credit")
	@PostMapping("/fintech/transaction-credit")
	public TransactioCreditResponse createAccount(@RequestBody TransactionCredit request) {
		
		TransactioCreditResponse transactioCreditResponse = transactionCreditService.createTransactionCreditResponse(request);
		return transactioCreditResponse;
	}
	
}
