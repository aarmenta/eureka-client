package com.metafinanciera.eurekaclient.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.metafinanciera.eurekaclient.entity.Account;
import com.metafinanciera.eurekaclient.model.AccountRequest;
import com.metafinanciera.eurekaclient.service.AccountService;

import io.swagger.annotations.ApiOperation;

@RestController
public class AccountController {
	
	@Autowired
	private AccountService accountService;
	
	@ApiOperation(value = "Crea un Account")
	@PostMapping("/fintech/alta-cuenta")
	public Account createAccount(@RequestBody AccountRequest request) {
		
		return accountService.generateAccount(request.getAccountInitialInput().getIdPerson(), request.getAccountInitialInput().getCreditLine());
		
	}

}
