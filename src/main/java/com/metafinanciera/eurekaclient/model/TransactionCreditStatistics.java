package com.metafinanciera.eurekaclient.model;

import java.util.UUID;

import javax.persistence.Column;

import com.google.gson.Gson;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TransactionCreditStatistics {
	
	@Column(nullable = false)
	private UUID processId;
	
	@Column(nullable = false)
	private String account;
	
	@Column(nullable = false)
	private String businessToken;
	
	@Column(nullable = false)
	private String pan;
	
	@Column(nullable = false)
	private String amount;
	
	@Column(nullable = false)
	private String commerceName;
	
	@Column(nullable = false)
	private String commerceId;
	
	@Column(nullable = false)
	private String messageType;
	
	@Column(nullable = false)
	private String authorizationDate;
	
	@Column(nullable = false)
	private String issuingCountry;
	
	@Column(nullable = false)
	private String issuingLanguage;
	
	@Column(nullable = false)
	private String cdt;
	
	@Column(nullable = false)
	private String cic;
	
	@Column(nullable = false)
	private String crt;
	
	@Column(nullable = false)
	private String tt;
	
	@Column(nullable = false)
	private String pco;
	
	@Column(nullable = false)
	private String pto;
	
	@Column(nullable = false)
	private String pos;
	
	@Column(nullable = false)
	private String mac;
	
	@Column(nullable = false)
	private String mad;
	
	@Column(nullable = false)
	private String mdc;
	
	@Column(nullable = false)
	private String ct;
	
	@Column(nullable = false)
	private String lp;
	
	@Column(nullable = false)
	private String ccp;
	
	@Column(nullable = false)
	private String cardSubtype;
	
	@Column(nullable = false)
	private String cardType;
	
	@Column(nullable = false)
	private String customer;
	
	@Column(nullable = false)
	private Double creditLine;
	
	public TransactionCreditStatistics(String account, String amount, String authorizationDate, String businessToken,
			String cardSubtype, String cardType, String ccp, String cdt, String cic, String commerceId, String commerceName,
			Double creditLine, String crt, String ct, String customer, String issuingCountry, String issuingLanguage,
			String lp, String mac, String mad, String mdc, String messageType, String pan, String pco, String pos, UUID processId,
			String pto, String tt) {
		this.account = account;
		this.amount = amount;
		this.authorizationDate = authorizationDate;
		this.businessToken = businessToken;
		this.cardSubtype = cardSubtype;
		this.cardType = cardType;
		this.ccp = ccp;
		this.cdt = cdt;
		this.cic = cic;
		this.commerceId = commerceId;
		this.commerceName = commerceName;
		this.creditLine = creditLine;
		this.crt = crt;
		this.ct = ct;
		this.customer = customer;
		this.issuingCountry = issuingCountry;
		this.issuingLanguage = issuingLanguage;
		this.lp = lp;
		this.mac = mac;
		this.mad = mad;
		this.mdc = mdc;
		this.messageType = messageType;
		this.pan = pan;
		this.pco = pco;
		this.pos = pos;
		this.processId = processId;
		this.pto = pto;
		this.tt = tt;
		
		
	}
	
	public String toJson() {
        return new Gson().toJson(this);
	}

}
