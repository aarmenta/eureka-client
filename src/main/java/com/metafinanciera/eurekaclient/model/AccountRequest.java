package com.metafinanciera.eurekaclient.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountRequest {
	
	@JsonProperty("eEntrada")
	private AccountInitialInput accountInitialInput;

}
