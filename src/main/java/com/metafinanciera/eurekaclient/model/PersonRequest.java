package com.metafinanciera.eurekaclient.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.metafinanciera.eurekaclient.entity.Person;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PersonRequest {

	@JsonProperty("eEntrada")
	private Person person;
}
