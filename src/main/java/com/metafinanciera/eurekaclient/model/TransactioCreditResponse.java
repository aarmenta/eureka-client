package com.metafinanciera.eurekaclient.model;

import java.util.UUID;

import javax.persistence.Column;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TransactioCreditResponse {
	
	@Column(nullable = false)
	private UUID processId;
	
	@Column(nullable = false)
	private String account;
	
	@Column(nullable = false)
	private String businessToken;
	
	@Column(nullable = false)
	private String status;
	
	public TransactioCreditResponse(UUID processId, String account, String businessToken, String status) {
		this.processId = processId;
		this.account = account;
		this.businessToken = businessToken;
		this.status = status;
	}

}
