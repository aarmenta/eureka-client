package com.metafinanciera.eurekaclient.model;


import javax.persistence.Column;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountInitialInput {
	
	@Column(nullable = false)
	private Long idPerson;
	
	@Column(nullable = false)
	private Double creditLine;

}
