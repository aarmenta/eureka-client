package com.metafinanciera.eurekaclient.service;

import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.metafinanciera.eurekaclient.entity.Account;
import com.metafinanciera.eurekaclient.entity.Person;
import com.metafinanciera.eurekaclient.entity.TransactionCredit;
import com.metafinanciera.eurekaclient.model.TransactioCreditResponse;
import com.metafinanciera.eurekaclient.model.TransactionCreditStatistics;
import com.metafinanciera.eurekaclient.repository.AccountRepository;
import com.metafinanciera.eurekaclient.repository.PersonRepository;
import com.metafinanciera.eurekaclient.repository.TransactionCreditRepository;

@Service
public class TransactionCreditService {
	
	@Autowired
	private AccountRepository accountRepository;
	
	@Autowired
	private PersonRepository personRepository;
	
	@Autowired
	private TransactionCreditRepository transactionCreditRepository;
	
	
	@Value("${transactioncreditstatistics.kafka.topic}")
	private String TRANSACTIONCREDITSTATISTICS_TOPIC;
	
	@Value("${transactioncreditstatisticsinvalid.kafka.topic}")
	private String TRANSACTIONCREDITSTATISTICSINVALID_TOPIC;
	
	@Autowired
    KafkaTemplate<String, Object> kafkaTemplate;
	
	public TransactioCreditResponse createTransactionCreditResponse(TransactionCredit request) {
		
		UUID idProcess = UUID.randomUUID();
		
		
		Optional<Account> account = accountRepository.findByAccountAndBusinessToken(request.getAccount(), request.getBusinessToken());
		request.setProcessId(idProcess);
		if(account.isPresent() == true) {
			Optional<Person> persona = personRepository.findById(account.get().getIdPerson());
			if(persona.isPresent() == true) {
				transactionCreditRepository.save(request);
				TransactioCreditResponse transactioCreditResponse = new TransactioCreditResponse(request.getProcessId(), request.getAccount(), request.getBusinessToken(), "Processed");
				sendKafkaTopicTransactionCredit(request, account, persona, "Valid");
				return transactioCreditResponse;
			}else {
				transactionCreditRepository.save(request);
				TransactioCreditResponse transactioCreditResponse = new TransactioCreditResponse(request.getProcessId(), request.getAccount(), request.getBusinessToken(), "Invalid Person");
				sendKafkaTopicTransactionCredit(request, account, persona, "Invalid");
				return transactioCreditResponse;
			}
			
		}else {
			transactionCreditRepository.save(request);
			Optional<Person> persona = personRepository.findById((long) 1);
			TransactioCreditResponse transactioCreditResponse = new TransactioCreditResponse(request.getProcessId(), request.getAccount(), request.getBusinessToken(), "Invalid Account");
			sendKafkaTopicTransactionCredit(request, account, persona, "Invalid");
			return transactioCreditResponse; 
		}
		
	}
	


	@Async
	public void sendKafkaTopicTransactionCredit(TransactionCredit transactionCredit, Optional<Account> account,
			Optional<Person> person, String status) {
		
		
	
		TransactionCreditStatistics transactionCreditStatistics = new TransactionCreditStatistics(account.get().getAccount(), 
					transactionCredit.getAmount(), 
					transactionCredit.getAuthorizationDate(), 
					account.get().getBusinessToken(), 
					"VIRTUAL",
					"CREDIT", 
					transactionCredit.getCcp(), 
					transactionCredit.getCdt(), 
					transactionCredit.getCic(),
					transactionCredit.getCommerceId(),
					transactionCredit.getCommerceName(),
					account.get().getCreditLine(), 
					transactionCredit.getCrt(), 
					transactionCredit.getCt(), 
					person.get().getName() + " " + person.get().getFirstSurname() + " " + person.get().getSecondSurname(), 
					transactionCredit.getIssuingCountry(), 
					transactionCredit.getIssuingLanguage(), 
					transactionCredit.getLp(), 
					transactionCredit.getMac(), 
					transactionCredit.getMad(), 
					transactionCredit.getMdc(), 
					transactionCredit.getMessageType(), 
					account.get().getPan(), 
					transactionCredit.getPco(), 
					transactionCredit.getPos(), 
					transactionCredit.getProcessId(), 
					transactionCredit.getPto(), 
					transactionCredit.getTt());
		
		if(status.equals("Invalid")) {
			kafkaTemplate.send(TRANSACTIONCREDITSTATISTICSINVALID_TOPIC,transactionCreditStatistics.toJson());
		}else {
			kafkaTemplate.send(TRANSACTIONCREDITSTATISTICS_TOPIC,transactionCreditStatistics.toJson());
		}
		
		
	}

}
