package com.metafinanciera.eurekaclient.service;

import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.metafinanciera.eurekaclient.entity.Account;
import com.metafinanciera.eurekaclient.entity.Person;
import com.metafinanciera.eurekaclient.repository.AccountRepository;
import com.metafinanciera.eurekaclient.repository.AccountStatistics;
import com.metafinanciera.eurekaclient.repository.PersonRepository;

import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class AccountService {
	
	
	@Autowired
	private AccountRepository accountRepository;
	
	@Autowired
	private PersonRepository personRepository;
	
	@Value("${accountstatistics.kafka.topic}")
	private String ACCOUNTSTATISTICS_TOPIC;
	
	@Autowired
    KafkaTemplate<String, Object> kafkaTemplate;

	public Account generateAccount(Long idPerson, Double creditLine) {
		
		Optional<Person> persona = personRepository.findById(idPerson);
		if(persona.isPresent() == true) {
			Account account = new Account();
			account.setAccount("00000" + (int)(Math.random() * 99999 +1));
			account.setBusinessToken("0019000100000000199800001000" + (int)(Math.random() * 999999998 + 1));
			account.setCreditLine(creditLine);
			account.setCvv("" + (int)(Math.random() * 999 + 1));
			account.setExpirationDate(new Date());
			account.setIdPerson(idPerson);
			account.setPan("439046******" + (int)(Math.random() * 9998 +1));
			
			log.info("Cuenta creada con id: {} y tokenNeogico: {}", account.getAccount(), account.getBusinessToken());
			Account cuenta = accountRepository.save(account);
			sendKafkaTopicAccount(cuenta, persona.get());
			return cuenta;
		}else {
			log.info("El id: {} no coincide con ningun registro en nuestras BD", idPerson);
			return null;
		}
	
		
	}
	
	@Async
	public void sendKafkaTopicAccount(Account account, Person person) {
		
		AccountStatistics accountStatistics = new AccountStatistics(person.getId(), account.getAccount(), account.getBusinessToken(), account.getPan(), account.getExpirationDate(), account.getCvv(), person.getName() + " " + person.getFirstSurname() + " " + person.getSecondSurname(), person.getIdentificationNumber());
		kafkaTemplate.send(ACCOUNTSTATISTICS_TOPIC,accountStatistics.toJson());
			
	}
	
	
}
