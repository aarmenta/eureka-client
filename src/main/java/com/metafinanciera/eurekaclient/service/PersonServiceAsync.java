package com.metafinanciera.eurekaclient.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.metafinanciera.eurekaclient.entity.Person;

@Service
public class PersonServiceAsync {
	
	@Autowired
    KafkaTemplate<String, Object> kafkaTemplate;
	
	@Value("${personstatistics.kafka.topic}")
	private String PERSONSTATISTICS_TOPIC;
	
	@Async
	public void sendKafkaTopicPerson(Person person) {
		kafkaTemplate.send(PERSONSTATISTICS_TOPIC,person.toJson());
	}

}
