package com.metafinanciera.eurekaclient.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import com.metafinanciera.eurekaclient.entity.Person;
import com.metafinanciera.eurekaclient.model.PersonRequest;
import com.metafinanciera.eurekaclient.repository.PersonRepository;

@Component
public class PersonService {

	@Autowired
	private PersonRepository personRepository;
	
	@Autowired
	private PersonServiceAsync personServiceAsync;
	
	
	public Person createPerson(PersonRequest request) {
		
		Person persona = personRepository.save(request.getPerson());
		personServiceAsync.sendKafkaTopicPerson(persona);
		
		return persona;
	}
	
	
	
}
